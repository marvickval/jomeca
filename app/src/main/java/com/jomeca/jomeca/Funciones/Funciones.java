package com.jomeca.jomeca.Funciones;

import android.app.Activity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

/**
 * Created by Mario-Mojica on 21/3/2020.
 */

public class Funciones {

    public Funciones(){}

    /**
     * FUNCION QUE MUESTRA UN LAYOUT EN FULL SCREEN*/
    public void FULLSCREEN(Activity activity) {
        activity.requestWindowFeature(Window.FEATURE_NO_TITLE);
        activity.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

}
