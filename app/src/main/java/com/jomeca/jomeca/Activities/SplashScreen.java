package com.jomeca.jomeca.Activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.jomeca.jomeca.Funciones.Funciones;
import com.jomeca.jomeca.R;

public class SplashScreen extends AppCompatActivity {

    ProgressBar pgB;
    ObjectAnimator progressAnimator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new Funciones().FULLSCREEN(this);
        setContentView(R.layout.activity_splash_screen);

        pgB = (ProgressBar) findViewById(R.id.pgBar);
        progressAnimator = ObjectAnimator.ofInt(pgB, "progress", 0, 100);

        progressAnimator.setDuration(10000);
        progressAnimator.start();
        progressAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
